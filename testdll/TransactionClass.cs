﻿using System.Collections.Generic;

public class TransactionClass
{
    public string Ping(Dictionary<string, string> dictionary)
    {
        string status = null;

        //сдесь реализовать проверку доступности поставщика
        //в поле message записать ответ о доступности поставщика. 
        //если поставщик доступен то ответ должен быть "ок" на латинице

        status = "ok";
        return "{ status: \"" + status + "\"}";
    }

    public string CheckUser(Dictionary<string, string> dictionary)
    {
        string username = null;
        string lastname = null;
        string middlename = null;
        string personalAccount = null;
        string status = null;

        //метод проверки пользователя у поставщика, 
        //реализовать метод по проверки на наличие пользователя у поставщика.
        //в метод приходит дикшенари с данными из филл дата. поля которые вы создаете при создании поставщика.
        //поля филл дата которые требуются для проверки пользователя.
        //для заполнения информации о пользователе заполнить соответствующие переменные.
        //сообщение status при положительном результатом заполняется "ок" на латинице

        return $"{{userName : \"{username}\",lastName: " +
                              $"\"{lastname}\",middleName: \"{middlename}" +
                              $"\",personalAccaunt: \"{personalAccount}\"" +
                              $",status: \"{status}\"}}";
    }

    public string Pay(Dictionary<string, string> dictionary)
    {
        string status = null;

        //сдесь реализовать метод по оплате услуг клиентом у поставщика
        //в поле message записать ответ о статусе проведении оплаты 
        //если оплата имеет положительный статус то в статус записать "Accepted" латинницей

        return "{ status: \"" + status + "\"}";
    }

    public string ChekPay(Dictionary<string, string> dictionary)
    {
        string status = null; // "Waiting" || "Success" || "Error" || "Return"

                              // "Waiting" если платеж принят но не проведен и не откланен
                              // "Success" если платеж успешно завершен
                              // "Error" если платеж был по какой либо причине откланен
                              
        string transactionId = dictionary["transactionId"]; // transactionId ключь с основного приложения

        //сдесь реализовать метод по проверке оплаты у поставщика
        //в поле message записать ответ о статусе проведении оплаты 
        //если оплата имеет положительный статус то в статус записать "ок" латинницей

        return "{ status: \"" + status + "\"}";
    }
}